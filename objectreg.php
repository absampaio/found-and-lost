
<?php // objectreg
include_once 'dbUtils.php';
include_once 'acesscontrol.php';



if (is_ajax()) {
  if (isset($_POST['action']) && !empty($_POST['action'])) { //Checks if action value exists
    $action = $_POST['action'];
    switch($action) { //Switch case for value of action
      case "lostobject": registerlost(); break;
      case "foundobject": registerfound();break;
    }
  }
}

//Function to check if the request is an AJAX request
function is_ajax() {
  return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}


function lostCount(){
    $currentUser = $_COOKIE['user_id'];
    $objnum = mysql_query("SELECT count(*) as total
        FROM user_lost AS UL
        JOIN lost_object AS LO ON UL.object_id = LO.object_id AND UL.user_id =$currentUser")or die(mysql_error());
        return mysql_result($objnum, 0);
}

function foundCount(){
    $currentUser = $_COOKIE['user_id'];
    $objnum = mysql_query("SELECT count(*) as total
        FROM user_found AS UL
        JOIN found_object AS LO ON UL.object_id = LO.object_id AND UL.user_id =$currentUser")or die(mysql_error());

       return mysql_result($objnum, 0);
}

function registerlost(){
    
    $formvars = $_POST;
    // Connects to Database 
    dbConnect('found_lost');

    if (!$formvars['title'] | !$formvars['description'] | !$formvars['tags'] | !$formvars['radius'] ) {

          die('You did not complete all of the required fields');

        }

    if(!$formvars['longitude']|!$formvars['latitude']){

        die('Failed to process coordinates, try again later');

    }

    $lat = $formvars['latitude'];
    $longt = $formvars['longitude'];

    $insertLoc = "INSERT INTO location(lat,longt)
                    VALUES ('$lat','$longt')";

     $add_location = mysql_query($insertLoc)or die(mysql_error());

     $locationQuery = mysql_query("SELECT location_id FROM location WHERE lat = $lat and longt = $longt ") or die(mysql_error());
     $locationID = -1;
     while ($row = mysql_fetch_array($locationQuery)) {
        $locationID = $row["location_id"];
     }

     $insertObj = "INSERT INTO lost_object(title, type, description,tags,location_id,radius)
            VALUES ('".$formvars['title']."','".$formvars['type']."','".$formvars['description']."','".$formvars['tags']."','$locationID','".$formvars['radius']."')";

     $add_object = mysql_query($insertObj)or die(mysql_error());

     $objectQuery = mysql_query("SELECT object_id FROM lost_object WHERE location_id = $locationID ") or die(mysql_error());

     $objectID = -1;
      while ($row = mysql_fetch_array($objectQuery)) {
        $objectID = $row["object_id"];
    }

     $insertUserObjRel = "INSERT INTO user_lost(user_id,object_id) VALUES ('".$_COOKIE['user_id']."','$objectID') ";

     $add_rel = mysql_query($insertUserObjRel)or die(mysql_error()); 

     header('Content-Type: application/json');
     $return["json"] = json_encode($return);
     $return ['lostcount'] = lostCount();
     $return ['objectID'] = $objectID;
     echo json_encode($return);   
}

function registerfound(){
    
    $formvars = $_POST;
    // Connects to Database 
    dbConnect('found_lost');

    if (!$formvars['title'] | !$formvars['description'] | !$formvars['tags'] ) {

          die('You did not complete all of the required fields');
        }

    if(!$formvars['longitude']|!$formvars['latitude']){

        die('Failed to process coordinates, try again later');
    }

    $lat = $formvars['latitude'];
    $longt = $formvars['longitude'];

    $insertLoc = "INSERT INTO location(lat,longt)
                    VALUES ('$lat','$longt')";

     $add_location = mysql_query($insertLoc)or die(mysql_error());

     $locationQuery = mysql_query("SELECT location_id FROM location WHERE lat = $lat and longt = $longt ") or die(mysql_error());
     $locationID = -1;
     while ($row = mysql_fetch_array($locationQuery)) {
        $locationID = $row["location_id"];
    }

     $insertObj = "INSERT INTO found_object(title,type,description,tags,location_id)
            VALUES ('".$formvars['title']."','".$formvars['type']."','".$formvars['description']."','".$formvars['tags']."','$locationID')";

     $add_object = mysql_query($insertObj)or die(mysql_error());

     $objectQuery = mysql_query("SELECT object_id FROM found_object WHERE location_id = $locationID ") or die(mysql_error());

     $objectID = -1;
      while ($row = mysql_fetch_array($objectQuery)) {
        $objectID = $row["object_id"];
    }

     $insertUserObjRel = "INSERT INTO user_found(user_id,object_id) VALUES ('".$_COOKIE['user_id']."','$objectID') ";

     $add_rel = mysql_query($insertUserObjRel)or die(mysql_error()); 


     $return = $_POST;
     
     header('Content-Type: application/json');
     $return["json"] = json_encode($return);
     $return ['foundcount'] = foundCount();
     $return ['objectID'] = $objectID;
     echo json_encode($return);    

}

?>
