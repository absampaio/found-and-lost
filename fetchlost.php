
<?php

include_once 'dbUtils.php';

function parseToXML($htmlStr)
{
	$xmlStr=str_replace('<','&lt;',$htmlStr);
	$xmlStr=str_replace('>','&gt;',$xmlStr);
	$xmlStr=str_replace('"','&quot;',$xmlStr);
	$xmlStr=str_replace("'",'&#39;',$xmlStr);
	$xmlStr=str_replace("&",'&amp;',$xmlStr);
	return $xmlStr;
}

dbConnect("found_lost");//connect to DB

$currentUser = intval($_COOKIE['user_id']);

// Select all the rows in the lost_objects table, that belongs to current user
$queryObjects = mysql_query("SELECT *
FROM user_lost AS UL
JOIN lost_object AS LO ON UL.object_id = LO.object_id AND UL.user_id =$currentUser")or die(mysql_error());

header("Content-type: text/xml");

// Start XML file, echo parent node
echo '<lost_objects>';


// Iterate through the rows, printing XML nodes for each
while ($row = mysql_fetch_array($queryObjects)){
  
  $locationID = $row['location_id'];
  $location = mysql_query("SELECT * FROM location WHERE location_id = $locationID")or die(mysql_error());
  
   while ($rowLoc = mysql_fetch_array($location)) {
        $lat = $rowLoc['lat'];
        $longt = $rowLoc['longt'];
    }

  // ADD TO XML DOCUMENT NODE

$title = parseToXML($row['title']);
$description = parseToXML($row['description']);
$tags = parseToXML($row['tags']);
$radius = $row['radius'];
$type = parseToXML($row['type']);
$objectID = parseToXML($row['object_id']);

  echo '<lost_object ';
  echo ' id = "'.$objectID.'"';
  echo ' title="'.$title. '"';
  echo ' description="'.$description. '"';
  echo ' tags= "'.$tags.'"';
  echo ' radius= "'.$radius.'"';
  echo ' lat= "'.$lat.'"';
  echo ' longt= "'.$longt.'"';
  echo ' type= "'.$type.'"';
  echo  "/> \n";

}

// End XML file
 echo '</lost_objects>';

?>