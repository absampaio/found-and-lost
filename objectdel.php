
<?php
include_once 'dbUtils.php';
include_once 'acesscontrol.php';

if (is_ajax()) {
  if (isset($_POST['action']) && !empty($_POST['action'])) { //Checks if action value exists
    $action = $_POST['action'];
    switch($action) { //Switch case for value of action
      case "lostobject": deletelost(); break;
      case "foundobject": deletefound();break;
    }
  }
}

//Function to check if the request is an AJAX request
function is_ajax() {
  return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}


function lostCount(){
    $currentUser = $_COOKIE['user_id'];
    $objnum = mysql_query("SELECT count(*) as total
        FROM user_lost AS UL
        JOIN lost_object AS LO ON UL.object_id = LO.object_id AND UL.user_id =$currentUser")or die(mysql_error());
     echo mysql_result($objnum, 0);
}

function foundCount(){
    $currentUser = $_COOKIE['user_id'];
    $objnum = mysql_query("SELECT count(*) as total
        FROM user_found AS UL
        JOIN found_object AS LO ON UL.object_id = LO.object_id AND UL.user_id =$currentUser")or die(mysql_error());

       echo mysql_result($objnum, 0);
}

function deletelost(){
    
    $formvars = $_POST;

    $object_id = $formvars['objectID'];
    // Connects to Database 
    dbConnect('found_lost');


    $deleteObject = "DELETE FROM lost_object where object_id = $object_id";//no need to delete user_found, delete is on cascade
    mysql_query($deleteObject)or die(mysql_error());

    echo lostCount();
}

function deletefound(){

    $formvars = $_POST;

    $object_id = $formvars['objectID'];
    // Connects to Database 
    dbConnect('found_lost');


    $deleteObject = "DELETE FROM found_object where object_id = $object_id";//no need to delete user_found, delete is on cascade
    mysql_query($deleteObject)or die(mysql_error());

     echo foundCount();

}

?>
