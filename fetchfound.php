
<?php

include_once 'dbUtils.php';

function parseToXML($htmlStr)
{
	$xmlStr=str_replace('<','&lt;',$htmlStr);
	$xmlStr=str_replace('>','&gt;',$xmlStr);
	$xmlStr=str_replace('"','&quot;',$xmlStr);
	$xmlStr=str_replace("'",'&#39;',$xmlStr);
	$xmlStr=str_replace("&",'&amp;',$xmlStr);
	return $xmlStr;
}

dbConnect("found_lost");//connect to DB

// Select all the rows in the lost_objects table
$queryObjects = mysql_query("SELECT title, FO.type, FO.description, FO.tags, FO.location_id, UF.object_id, UF.user_id, U.user_name
FROM found_object AS FO
JOIN user_found AS UF ON FO.object_id = UF.object_id
JOIN user AS U ON U.user_id = UF.user_id")or die(mysql_error());


header("Content-type: text/xml");

// Start XML file, echo parent node
echo '<found_objects>';


// Iterate through the rows, printing XML nodes for each
while ($row = mysql_fetch_array($queryObjects)){
  
  $locationID = $row['location_id'];
  $location = mysql_query("SELECT * FROM location WHERE location_id = $locationID")or die(mysql_error());
  
   while ($rowLoc = mysql_fetch_array($location)) {
        $lat = $rowLoc['lat'];
        $longt = $rowLoc['longt'];
    }

  // ADD TO XML DOCUMENT NODE

$title = parseToXML($row['title']);
$description = parseToXML($row['description']);
$tags = parseToXML($row['tags']);
$type = parseToXML($row['type']);
$userName = parseToXML($row['user_name']);
$objectID = parseToXML($row['object_id']);

  echo '<found_object ';
  echo ' foundby= "'.$userName.'"';
  echo ' id = "'.$objectID.'"';
  echo ' title= "'.$title. '"';
  echo ' description= "'.$description. '"';
  echo ' tags= "'.$tags.'"';
  echo ' lat= "'.$lat.'"';
  echo ' longt= "'.$longt.'"';
  echo ' type= "'.$type.'"';
  echo  "/> \n";

}

// End XML file
 echo '</found_objects>';

?>