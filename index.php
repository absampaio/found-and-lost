
 <?php 
   include 'acesscontrol.php';
  include 'objectedit.php';
   acesslogin();
  if(isset($_GET['logout'])){
    acesslogout();
  }
  ?>
<!DOCTYPE html>
<html>
  <head>
  <meta charset="utf-8">  
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
     <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
  </head>
  <body>
  <div id="map-canvas"></div>
    <div id="search_results" class="container">
      <div class="row">
      <div class="col-md-3 col-md-offset-9">
        <div class="panel">
          <div class="panel-heading remove-mp-bottom">
            <button type="button" class="close" onclick="hideResults()" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="row">
              <div class="col-xs-12">
                <h4 >Matching objects</h4>
                <hr class="remove-mp-bottom">
              </div>
            </div>
          </div>
          <div class="panel-body panel-size">
            <div class="row">
              <div class="col-lg-12" id="results">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a onclick="goHome()" class="navbar-brand" href="#"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
      <li><a onclick="inputFile()" href="#"><span class="glyphicon glyphicon-import" aria-hidden="true"></span> Import XML</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Filter Objects <span class="caret"></span></a>
          <ul class="dropdown-menu dropdown-menu-form" role="menu">
            <li><a href="#"><label>
      <input type="checkbox" value = "0" onclick="filterMarkers(this, this.value)" checked> Electronics
    </label></a></li>
            <li><a href="#"><label>
      <input type="checkbox" value = "1" checked onclick="filterMarkers(this, this.value)"> Clothes
    </label></a></li>
    <li><a href="#"><label>
      <input type="checkbox" value = "2" onclick="filterMarkers(this, this.value)" checked> Wallets/Purses
    </label></a></li>
    <li><a href="#"><label>
      <input type="checkbox" value = "3" onclick="filterMarkers(this, this.value)" checked> Keys
    </label></a></li>
          </ul>
        </li>
        <li id="search_tab" onclick="showSearch()"><a href="#"><span class="glyphicon glyphicon-search"></span></a></li>
      </ul>
     
      <ul class="nav navbar-nav navbar-right">
        <li id="lost_tab" class="active" onclick="changeMode(0)"><a href="#">Lost(<?php lostCount() ?>)</a></li>
        <li id="found_tab" onclick="changeMode(1)"><a href="#">Found(<?php foundCount() ?>)</a></li>
        <li><a onclick = "bullseyeOn()" href="#"><i class="fa fa-bullseye fa-3 "></i></a></li>
        <li><a href="#"><?= $_COOKIE['username'] ?></a></li>
        <li><a href="index.php?logout=true">Logout</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<!-- Modal contact -->
<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Contact Adama Candé</h4>
      </div>
      <div class="modal-body">
        <button class="btn btn-success">Contact by Phone</button>
        <button class="btn btn-primary">Contact by Email</button>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal input file -->
<div class="modal fade" id="inputModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Select XML File</h4>
      </div>
      <div class="modal-body">
        <input id="file_path" type="file">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" onclick="uploadXML()" data-dismiss="modal">Upload</button>
      </div>
    </div>
  </div>
</div>
  </body>
  <script src="js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script src="js/bootstrap.min.js"></script>
</html>