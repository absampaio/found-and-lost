-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 25, 2015 at 12:25 PM
-- Server version: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `found_lost`
--

USE found_lost;


--
-- Table structure for table `found_object`
--

CREATE TABLE IF NOT EXISTS `found_object` (
  `title` varchar(100) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text NOT NULL,
  `tags` text NOT NULL,
  `location_id` int(11) NOT NULL,
  `object_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`object_id`),
  KEY `location_id` (`location_id`)

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location` (
  `lat` double NOT NULL,
  `longt` double NOT NULL,
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Table structure for table `lost_object`
--

CREATE TABLE IF NOT EXISTS `lost_object` (
  `title` varchar(100) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text NOT NULL,
  `tags` text NOT NULL,
  `location_id` int(11) NOT NULL,
  `radius` double NOT NULL,
  `object_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`object_id`),
  KEY `location_id` (`location_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE IF NOT EXISTS `type` (
  `type` int(11) NOT NULL,
  `icon` blob NOT NULL,
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `mail` varchar(100) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_real_name` varchar(100) NOT NULL,
  `phone_number` varchar(100) NOT NULL,
  `count_lost` int(11) NOT NULL,
  `count_found` int(11) NOT NULL,
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;
-- --------------------------------------------------------

--
-- Table structure for table `user_found`
--

CREATE TABLE IF NOT EXISTS `user_found` (
  `user_id` int(11) NOT NULL,
  `object_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`object_id`),
  KEY `object_id` (`object_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_lost`
--

CREATE TABLE IF NOT EXISTS `user_lost` (
  `user_id` int(11) NOT NULL,
  `object_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`object_id`),
  KEY `object_id` (`object_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `found_object`
--
ALTER TABLE `found_object`
  ADD CONSTRAINT `found_object_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `location` (`location_id`) ON DELETE CASCADE;

--
-- Constraints for table `lost_object`
--
ALTER TABLE `lost_object`
  ADD CONSTRAINT `lost_object_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `location` (`location_id`) ON DELETE CASCADE;

--
-- Constraints for table `user_found`
--
ALTER TABLE `user_found`
  ADD CONSTRAINT `user_found_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_found_ibfk_2` FOREIGN KEY (`object_id`) REFERENCES `found_object` (`object_id`) ON DELETE CASCADE;

--
-- Constraints for table `user_lost`
--
ALTER TABLE `user_lost`
  ADD CONSTRAINT `user_lost_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_lost_ibfk_2` FOREIGN KEY (`object_id`) REFERENCES `lost_object` (`object_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
