$(function() {

    $('#login-form-link').click(function(e) {
		$("#login-form").delay(100).fadeIn(100);
 		$("#register-form").fadeOut(100);
		$('#register-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#register-form-link').click(function(e) {
		$("#register-form").delay(100).fadeIn(100);
 		$("#login-form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});

});

function initialize() {
  var mapOptions = {
    zoom: 8,
    center: new google.maps.LatLng(38.983090, -9.117088)
  };

  map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);

   map.setOptions({draggable: false, 
        zoomControl: false, 
        scrollwheel: false, 
        disableDoubleClickZoom: true,
        streetViewControl: false,
        panControl: false,
        mapTypeControl:false});
}

function loadScript() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyB0t00mazIDspCaqJ2v_piUXSFbYEErfmo&callback=initialize';
  document.body.appendChild(script);
}

window.onload = loadScript;