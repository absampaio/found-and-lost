var currentPos;
var map;
var openInfoWindow;
var lostMarkers = [];
var foundMarkers = [];
var notifications = [];
var circles = [];
var currentMode = 0; // 0 = LOST && 1 = FOUND && 2 = SEARCH
var circle;
var cRadius;
var sCircle;
var showingSearch = 0;
//var idMarker = 0;
var drawingManager;
var currentMarker = null;
var sPolygon;

jQuery.fn.visible = function() {
    return this.css('visibility', 'visible');
};

jQuery.fn.invisible = function() {
    return this.css('visibility', 'hidden');
};

jQuery.fn.visibilityToggle = function() {
    return this.css('visibility', function(i, visibility) {
        return (visibility == 'visible') ? 'hidden' : 'visible';
    });
};

$(document).ready(function(){
  $("#search_results").invisible();
  $("#search_results").invisible();
  $('.dropdown-menu').on('click', function(e) {
    if($(this).hasClass('dropdown-menu-form')) {
        e.stopPropagation();
    }

    document.getElementById('file_path')
  .addEventListener('change', uploadXML, false);
});

});

function initialize() {
  var mapOptions = {
    zoom: 8,
    center: new google.maps.LatLng(38.983090, -9.117088)
  };

  map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);

  openInfoWindow = new google.maps.InfoWindow();

  google.maps.event.addListener(map, 'click', function(event) {
   placeInfo(event.latLng); 
});

if (!google.maps.Polyline.prototype.getBounds)
google.maps.Polyline.prototype.getBounds = function() {
   
  var bounds = new google.maps.LatLngBounds();
    
  this.getPath().forEach( function(latlng) { bounds.extend(latlng); } ); 
   
  return bounds; 
   
}

drawingManager = new google.maps.drawing.DrawingManager({
    drawingControl: false,
    drawingControlOptions: {
      position: google.maps.ControlPosition.TOP_CENTER,
      drawingModes: [
        google.maps.drawing.OverlayType.CIRCLE,
        google.maps.drawing.OverlayType.POLYLINE
      ]
    },
    circleOptions: {
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      clickable: false,
      editable: false,
      zIndex: 1
    },
    polylineOptions: {
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      clickable: false,
      editable: false,
      zIndex: 1
    }
  });

    google.maps.event.addListener(drawingManager, 'circlecomplete', function(newcircle) {
      clearPolygon(sPolygon);
  clearCircle(sCircle);
  searchByRadius(newcircle.getRadius(), newcircle.getCenter());
  map.fitBounds(newcircle.getBounds());
  sCircle = newcircle;
  $("#search_results").visible();
});

google.maps.event.addListener(drawingManager, 'polylinecomplete', function(polygon){
  clearCircle(sCircle);
  clearPolygon(sPolygon);
  searchByPath(polygon);
  map.fitBounds(polygon.getBounds());
  sPolygon = polygon;
  
  $("#search_results").visible();

});

   // Try HTML5 geolocation
  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      currentPos = new google.maps.LatLng(position.coords.latitude,
                                       position.coords.longitude);

      map.setCenter(currentPos);
      map.setZoom(15);
    }, function() {
      handleNoGeolocation(true);
    });
  } else {
    // Browser doesn't support Geolocation
    handleNoGeolocation(false);
  }

downloadUrl("fetchlost.php", function(data) {
  
      var xml = data.responseXML;
      var markers = xml.documentElement.getElementsByTagName("lost_object");
      for (var i = 0; i < markers.length; i++) {
        var objectID = markers[i].getAttribute("id");
        var title = markers[i].getAttribute("title");
        var type = markers[i].getAttribute("type");
        var description = markers[i].getAttribute("description");
        var tags = markers[i].getAttribute("tags");
        var radius = markers[i].getAttribute("radius");
        var lat = markers[i].getAttribute("lat");
        var longt = markers[i].getAttribute("longt");
        
        drawCircle(lat,longt,radius,true);
        placeLostMarker(objectID,lat,longt,title,type,description,tags,radius);
        
      }

});


downloadUrl("fetchfound.php", function(data) {
  
      var xml = data.responseXML;
      var markers = xml.documentElement.getElementsByTagName("found_object");
      for (var i = 0; i < markers.length; i++) {
        var objectID = markers[i].getAttribute("id");
        var title = markers[i].getAttribute("title");
        var type = markers[i].getAttribute("type");
        var username = markers[i].getAttribute("foundby");
        var description = markers[i].getAttribute("description");
        var tags = markers[i].getAttribute("tags"); 
        var lat = markers[i].getAttribute("lat");
        var longt = markers[i].getAttribute("longt");
        placeFoundMarker(objectID,lat,longt,title,type,description,tags,username);
      }
      poll();
});

}

function poll() {
   setTimeout(function() {
           notifyByRadius();
           poll();
    }, 3000);
}

function clearCircle(circle){
  if (typeof circle !== "undefined" && circle !== null){
    circle.setMap(null);
  }
}

function showCircle(circle){
  circle.setMap(map);
}

function clearPolygon(polygon){
  if (typeof polygon !== "undefined" && polygon !== null){
    polygon.setMap(null);
  }
}


function showSearch(){
  if (!showingSearch){
      drawingManager.setMap(map);
// To show:
drawingManager.setOptions({
  drawingControl: true
});
showingSearch = 1;
google.maps.event.clearListeners(map, 'click');

  } else {
      drawingManager.setMap(null);
     // To hide:
drawingManager.setOptions({
  drawingControl: false
});
showingSearch = 0;
google.maps.event.addListener(map, 'click', function(event) {
   placeInfo(event.latLng);
});
  }
}


function notifyByRadius(){
  
  for(var i = 0; i < lostMarkers.length; i++){
    for(var j = 0; j < foundMarkers.length; j++){
      if(!foundMarkers[j]['polled'])
        if ( lostMarkers[i].type == foundMarkers[j].type && google.maps.geometry.spherical.computeDistanceBetween(foundMarkers[j].getPosition(),lostMarkers[i].getPosition()) < lostMarkers[i]['radius']){
          notifications.push(foundMarkers[j]);
          foundMarkers[j]['polled'] = 1;
    }
  }
}
  addToNotifications();
}

function addToNotifications(){
   $(".fa").html("");
   $("<span class='icon_counter icon_counter_green'>"+notifications.length+"</span>").appendTo(".fa"); 
}

function bullseyeOn(){
  $("#search_results").visible();
  addToResultsBullseye();
}

function bullseyeOff(){
  $("#search_results").invisible();
}

function addToResultsBullseye(){
  $("#results").html("");
  $.each(notifications, function(i,marker){
    $('<div onclick = centerTo('+marker['lat']+','+marker['lng']+') id = marker'+marker['id']+' class="alert alert-success search-object" role="alert"><a href = "#" class="fill-div" ><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> ' +  $(marker['infowindow'].content).html() + '</a></div>').appendTo("#results");
    $( "#marker"+marker['id']).click(function() {
     toggleBounce(marker);
    });
  });
}


function downloadUrl(url,callback) {
 var request = window.ActiveXObject ?
     new ActiveXObject('Microsoft.XMLHTTP') :
     new XMLHttpRequest;

 request.onreadystatechange = function() {
   if (request.readyState == 4) {
     //request.onreadystatechange = doNothing;
     callback(request, request.status);
   }
 };

 request.open('GET', url, true);
 request.send(null);
}


function addToResults(result){
  $("#results").html("");
  $.each(result, function(i,marker){
    if (!marker['mType']){
      $('<div class="alert alert-danger search-object" role="alert"><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> ' +  $(marker['infowindow'].content).html() + '</div>').appendTo("#results");
    } else {
      $('<div class="alert alert-success search-object" role="alert"><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> ' +  $(marker['infowindow'].content).html() + '</div>').appendTo("#results");
    }
      
});

}

function hideResults(){
 $("#results").append("");
 $("#search_results").invisible();
}

function centerTo(lat,lng){
   map.setCenter(new google.maps.LatLng(lat,lng));
   map.setZoom(15);
}

function toggleBounce(marker) {
  if (currentMarker != null) {
    untoggleBounce();
    currentMarker = null;
    toggleBounce(marker);
  }else {
    currentMarker = marker;
    currentMarker.setAnimation(google.maps.Animation.BOUNCE);
    setTimeout(function(){untoggleBounce(currentMarker)}, 30000);
  }
}

function untoggleBounce(){
  if(currentMarker != null){
    currentMarker.setAnimation(null);
    currentMarker = null;
  }
}

function searchByRadius(radius, center){
  var in_area = [];
  $.each(foundMarkers, function(i,marker) {
    if (google.maps.geometry.spherical.computeDistanceBetween(marker.getPosition(), center) < radius){
      in_area.push(marker);
    }
  });

  $.each(lostMarkers, function(i,marker) {
    if (google.maps.geometry.spherical.computeDistanceBetween(marker.getPosition(), center) < radius){
      in_area.push(marker);
    }
  });

  addToResults(in_area);
  fitBoundsToMarkers(in_area);
}

function fitBoundsToMarkers(markers) {

    var bounds = new google.maps.LatLngBounds();

    for (var i=0; i<markers.length; i++) {
        if(markers[i].getVisible()) {
            bounds.extend( markers[i].getPosition() );
        }
    }

    map.fitBounds(bounds);

}

function searchByPath(polygon){
  var in_area = [];

  $.each(foundMarkers, function(i,marker) {
    if (polygon.getBounds().contains(marker.getPosition())){
      in_area.push(marker);
    }
  });

  $.each(lostMarkers, function(i,marker) {
    if (polygon.getBounds().contains(marker.getPosition())){
      in_area.push(marker);
    }
  });

  addToResults(in_area);

  fitBoundsToMarkers(in_area);
}

function getMarkerCircle(center){
  for (var i = 0; i < circles.length; i++){
        if (circles[i].getCenter().lat() == center.lat() && circles[i].getCenter().lng() == center.lng() ){
          return circles[i];
        }
  }
  return null;
}


function filterMarkers(element, type){
    var visible = [];
    if (element.checked){
       $.each(lostMarkers, function(i,marker) {
          if (marker['type'] == type){
            marker.setVisible(true);
            showCircle(getMarkerCircle(marker.getPosition())); 
          }
       });
       $.each(foundMarkers, function(i,marker) {
          if (marker['type'] == type){
            marker.setVisible(true);
          }
       });
    } else {
        $.each(lostMarkers, function(i,marker) {
          if (marker['type'] == type){
            marker.setVisible(false);
            clearCircle(getMarkerCircle(marker.getPosition())); 
          }
       });
       $.each(foundMarkers, function(i,marker) {
          if (marker['type'] == type){
            marker.setVisible(false);
          }
       });
    }

    $.each(lostMarkers, function(i,marker) {
          if (marker.getVisible()){
            visible.push(marker);
          }
       });
       $.each(foundMarkers, function(i,marker) {
          if (marker.getVisible()){
            visible.push(marker);
          }
    });

    if (visible.length > 0)
      fitBoundsToMarkers(visible);
}

function changeLostCount(value){

  $('#lost_tab').text("");
  $('#lost_tab').append("<a href='#'>Lost(" + value + ")</a>");  
} 

function changeFoundCount(value){
  $('#found_tab').text("");
  $('#found_tab').append("<a href='#'>Found(" + value + ")</a>");  
} 

function changeMode(mode){
  clearCircle(sCircle);
  clearPolygon(sPolygon);
  currentMode = mode;
  if (showingSearch){
    if (mode < 2){
      showSearch();
    }
    $("#search_results").invisible();
    $("#search_results").invisible();

  }
  if (mode){
    $('#found_tab').addClass("active");
    $('#lost_tab').removeClass("active");
    $('#search_tab').removeClass("active");
    showFoundPanel();
   // clearCircle(circle);
    
  } else if (mode == 0) {
    $('#lost_tab').addClass("active");
    $('#found_tab').removeClass("active");
    $('#search_tab').removeClass("active");
    showLostPanel();
  } else {
    $('#found_tab').removeClass("active");
    $('#lost_tab').removeClass("active");
    $('#search_tab').addClass("active");
  }

     if (typeof openInfoWindow !== "undefined" && openInfoWindow !== null && isInfoWindowOpen(openInfoWindow)){
        openInfoWindow.close();
    }
  
}

function isInfoWindowOpen(infoWindow){
    var map = infoWindow.getMap();
   // clearCircle(circle);
    return (map !== null && typeof map !== "undefined");
}


function placeInfo(location) {

    if (typeof openInfoWindow !== "undefined" && openInfoWindow !== null && isInfoWindowOpen(openInfoWindow)){
        openInfoWindow.close();
    }

    if (currentMode == 1){
      openInfoWindow = new google.maps.InfoWindow({
        map: map,
        position: location,
        content: "<form>"
        +"<div class='form-group'>"
        +"<input placeholder='title' type='text' class='form-control' id='title'>"
        +"</div>"
        +"<div class='form-group'>"
        +"<select name='type' id='type' class='form-control'>"
        +"<option value='0' selected disabled>Type of object</option>"
        +"<option value='0'>Electronics</option>"
        +"<option value='1'>Clothes</option>"
        +"<option value='2'>Wallets/Purses</option>"
        +"<option value='3'>Keys</option>"
        +"</select>"
        +"</div>"
        +"<div class='form-group'>"
        +"<input placeholder='tags' type='text' class='form-control' id='tags'>"
        +"</div>"
        +"<div class='form-group'>"
        +"<textarea placeholder='description' style='resize:none' class='form-control' rows='5' id='description'></textarea>"
        +"</div>"
        +"<input type='hidden' name = 'latitude' class = 'latitude form-control' id = 'latitude' value ="+location.lat()+" />"
        +"<input type='hidden' name = 'longitude' class = 'longitude form-control' id = 'longitude' value ="+location.lng()+" />"
        +"<button type='button' id='insert_found' class='btn btn-success btn-block'>Insert Found Marker</button>"
        +"</form>"
      });
    } else {
      openInfoWindow = new google.maps.InfoWindow({
        map: map,
        position: location,
        content: "<form>"
        +"<div class='form-group'>"
        +"<input type='text' class = 'title form-control' name = 'title' placeholder='title'  id='title'>"
        +"</div>"
        +"<div class='form-group'>"
        +"<select name='type' id='type' class='form-control'>"
        +"<option value='0' selected disabled>Type of object</option>"
        +"<option value='0'>Electronics</option>"
        +"<option value='1'>Clothes</option>"
        +"<option value='2'>Wallets/Purses</option>"
        +"<option value='3'>Keys</option>"
        +"</select>"
        +"</div>"
        +"<div class='form-group'>"
        +"<input type='text' class = 'tags form-control' name = 'tags' placeholder='tags' id='tags'>"
        +"</div>"
        +"<div class='form-group'>"
        +"<textarea name = 'description' class = 'description form-control' placeholder='description' style='resize:none' rows='5' id='description'></textarea>"
        +"</div>"
        +"<div class='form-group'>"
        +"<input type='text' name='radius' class = 'radius form-control' placeholder='search radius in meters' class='form-control' id='radius' onkeyup='drawCircle(" + location.lat() + "," + location.lng() + "," + 'this.value' + "," + false + ")'>"
        +"</div>"
        +"<input type='hidden' name = 'latitude' class = 'latitude form-control' id = 'latitude' value ="+location.lat()+" />"
        +"<input type='hidden' name = 'longitude' class = 'longitude form-control' id = 'longitude' value ="+location.lng()+" />"
        +"<input type='button' name='submit_lost' id='insert_lost'  class='btn btn-success btn-block' value = 'Insert Lost Object'/>"
        +"</form>"
      });
    }

    google.maps.event.addListener(openInfoWindow, 'domready', function(){
         var title = $('#title');
         var tags = $('#tags');
         var desc = $('#description');
         var radius = $('#radius');
         var lat = $('#latitude');
         var lng = $('#longitude');
         var type = $('#type');

         if (currentMode == 0){
            $('#insert_lost').click(function() {
          insertLostInDb(title.val(),type.find(":selected").val(),desc.val(),tags.val(),radius.val(),lat.val(),lng.val());
         
         // alert(objectID);
          //placeLostMarker(objectID,location.lat() ,location.lng(),title.val(),type.find(":selected").val(),desc.val(),tags.val(),radius.val());
          
         });
       } else {
        $('#insert_found').click(function() {
          insertFoundInDb(title.val(),type.find(":selected").val(),desc.val(),tags.val(),lat.val(),lng.val());
          //alert(objectID);
          //placeFoundMarker(objectID,location.lat(),location.lng(),title.val(),type.find(":selected").val(),desc.val(),tags.val(),getCookie("username"));
         });
          
         }
         
});


  google.maps.event.addListener(openInfoWindow,'closeclick',function(){
   clearCircle(circle);
});
}

function insertLostInDb(title,type,description,tags,radius,lat,lng){

                    var data = {
                      "action": "lostobject",
                      "title":  title,
                      "type" : type,
                      "description": description,
                      "tags": tags,
                       "radius": radius,
                       "latitude" : lat,
                       "longitude" : lng
                    };
                   data = $(this).serialize() + "&" + $.param(data);
                  $.ajax({
                      type: "POST",
                      dataType: "json",
                      url: "./objectreg.php", //Relative or absolute path to response.php file
                      data: data,
                      success:function(data) {
                        changeLostCount(data['lostcount']);
                        clearCircle(circle);
                        drawCircle(lat,lng,radius,true);
                        placeLostMarker(data['objectID'], lat ,lng,title,type,description,tags,radius);
                      }
                     });
}

function insertFoundInDb(title,type,description,tags,lat,lng){

                    var data = {
                      "action": "foundobject",
                      "title":  title,
                      "type": type,
                      "description": description,
                      "tags": tags,
                       "latitude" : lat,
                       "longitude" : lng
                    };
                   data = $(this).serialize() + "&" + $.param(data);
                   $.ajax({
                      type: "POST",
                      dataType: "json",
                      url: "./objectreg.php", //Relative or absolute path to response.php file
                      data: data,
                      success:function(data) {
                        changeFoundCount(data['foundcount']);
                        placeFoundMarker(data['objectID'],lat,lng,title,type,description,tags,getCookie("username"));
                      }
                     });
                   
}

function editLostInDb(marker,title,type,description,tags,radius,lat,lng){

                    var data = {
                      "action": "lostobject",
                      "objectID":marker['id'],
                      "title":  title,
                      "type" : type,
                      "description": description,
                      "tags": tags,
                       "radius": radius,
                       "latitude" : lat,
                       "longitude" : lng
                    };
                   data = $(this).serialize() + "&" + $.param(data);
                    $.ajax({
                      type: "POST",
                      dataType: "json",
                      url: "./objectedit.php", //Relative or absolute path to response.php file
                      data: data,
                      success:function(data) {                            
                      }
                     });
}

function editFoundInDb(marker,title,type,description,tags,lat,lng){

                    var data = {
                      "action": "foundobject",
                      "objectID":marker['id'],
                      "title":  title,
                      "type": type,
                      "description": description,
                      "tags": tags,
                       "latitude" : lat,
                       "longitude" : lng
                    };
                   data = $(this).serialize() + "&" + $.param(data);
                    $.ajax({
                      type: "POST",
                      dataType: "json",
                      url: "./objectedit.php", //Relative or absolute path to response.php file
                      data: data,
                      success:function(data) { 
                      }
                     });
                    
}

function removeMarker(marker){
  marker.setMap(null);
  for (var i = 0; i < markers.length; i++) {
      if (marker == markers[i]){
        markers[i].splice(i,1);
        break;
      }
  }

}

function deleteFoundInDb(marker){

                    var data = {
                      "action": "foundobject",
                      "objectID":marker['id']
                    };
                   data = $(this).serialize() + "&" + $.param(data);
                    $.ajax({
                      type: "POST",
                      dataType: "json",
                      url: "./objectdel.php", //Relative or absolute path to response.php file
                      data: data,
                      success:function(data) {
                        openInfoWindow.close();
                        changeFoundCount(data);
                        removeMarker(marker);    
                      }
                     });
                    

}

function deleteLostInDb(marker){

                    var data = {
                      "action": "lostobject",
                      "objectID":marker['id']
                    };
                   data = $(this).serialize() + "&" + $.param(data);
                    $.ajax({
                      type: "POST",
                      dataType: "json",
                      url: "./objectdel.php", //Relative or absolute path to response.php file
                      data: data,
                      success:function(data) {
                          openInfoWindow.close();
                          changeLostCount(data); 
                          clearCircle(getMarkerCircle(marker.getPosition())); 
                          removeMarker(marker);
                      }
                     });
                    
}

function showLostPanel(){
  //alert("hue");
   $("#search_results").visible();
  $("#results").html("");
  $.each(lostMarkers, function(i,marker){
    $('<div onclick = centerTo('+marker['lat']+','+marker['lng']+') id = marker'+marker['id']+' class="alert alert-success search-object" role="alert"><a href = "#" class="fill-div" ><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> ' +  $(marker['infowindow'].content).html() + '</a></div>').appendTo("#results");
    $( "#marker"+marker['id']).click(function() {
     openEditWindow(marker);
    });
  });
}


function showFoundPanel(){
  //alert("hue");
   $("#search_results").visible();
  $("#results").html("");
  $.each(foundMarkers, function(i,marker){
    if (marker['user'] == getCookie("username")){
      $('<div onclick = centerTo('+marker['lat']+','+marker['lng']+') id = marker'+marker['id']+' class="alert alert-success search-object" role="alert"><a href = "#" class="fill-div" ><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> ' +  $(marker['infowindow'].content).html() + '</a></div>').appendTo("#results");
    }
    $( "#marker"+marker['id']).click(function() {
     openEditWindow(marker);
    });
  });
}


function openEditWindow(marker){

  if(openInfoWindow != null){
    openInfoWindow.close();
  }

  if (marker['mType'] == 0){
    openInfoWindow = new google.maps.InfoWindow({
    map: map,
    position: new google.maps.LatLng(marker['lat'],marker['lng']),
    content: "<form >"
        +"<div class='form-group'>"
        +"<input type='text' value=" + marker['title'] + " class = 'title form-control' name = 'title' placeholder='title'  id='title'>"
        +"</div>"
        +"<div class='form-group'>"
        +"<select name='type' id='type' class='form-control'>"
        +"<option value='0' selected disabled>Type of object</option>"
        +"<option value='0'>Electronics</option>"
        +"<option value='1'>Clothes</option>"
        +"<option value='2'>Wallets/Purses</option>"
        +"<option value='3'>Keys</option>"
        +"</select>"
        +"</div>"
        +"<div class='form-group'>"
        +"<input type='text' value=" + marker['tags'] + " class = 'tags form-control' name = 'tags' placeholder='tags' id='tags'>"
        +"</div>"
        +"<div class='form-group'>"
        +"<textarea name = 'description' class = 'description form-control' placeholder='description' style='resize:none' rows='5' id='description'>" + marker['description']+"</textarea>"
        +"</div>"
        +"<div class='form-group'>"
        +"<input type='text' name='radius' value=" + marker['radius'] + " class = 'radius form-control' placeholder='search radius in meters' class='form-control' id='radius' onkeyup='drawCircle(" + marker['lat'] + "," + marker['lng'] + "," + 'this.value' + "," + false + ")'>"
        +"</div>"
        +"<input type='hidden' name = 'latitude' class = 'latitude form-control' id = 'latitude' value ="+marker['lat']+" />"
        +"<input type='hidden' name = 'longitude' class = 'longitude form-control' id = 'longitude' value ="+marker['lng']+" />"
        +"<input type='button' name='submit_lost' id='delete_lost'  class='btn btn-danger btn-block' value = 'Delete Lost Object'/>"
        +"<input type='button' name='submit_lost' id='edit_lost'  class='btn btn-success btn-block' value = 'Edit Lost Object'/>"
        +"</form>"
    }); 
  } else {
    openInfoWindow = new google.maps.InfoWindow({
    map: map,
    position: new google.maps.LatLng(marker['lat'],marker['lng']),
    content: "<form>"
        +"<div class='form-group'>"
         +"<input type='text' value=" + marker['title'] + " class = 'title form-control' name = 'title' placeholder='title'  id='title'>"
        +"</div>"
        +"<div class='form-group'>"
        +"<select name='type' id='type' class='form-control'>"
        +"<option value='0' selected disabled>Type of object</option>"
        +"<option value='0'>Electronics</option>"
        +"<option value='1'>Clothes</option>"
        +"<option value='2'>Wallets/Purses</option>"
        +"<option value='3'>Keys</option>"
        +"</select>"
        +"</div>"
        +"<div class='form-group'>"
        +"<input type='text' value=" + marker['tags'] + " class = 'tags form-control' name = 'tags' placeholder='tags' id='tags'>"
        +"</div>"
        +"<div class='form-group'>"
        +"<textarea name = 'description' class = 'description form-control' placeholder='description' style='resize:none' rows='5' id='description'>" + marker['description']+"</textarea>"
        +"</div>"
        +"<input type='hidden' name = 'latitude' class = 'latitude form-control' id = 'latitude' value ="+marker['lat']+" />"
        +"<input type='hidden' name = 'longitude' class = 'longitude form-control' id = 'longitude' value ="+marker['lng']+" />"
        +"<button type='button' id='delete_found' class='btn btn-danger btn-block'>Delete Found Marker</button>"
        +"<button type='button' id='edit_found' class='btn btn-success btn-block'>Edit Found Marker</button>"
        +"</form>"
  }); 
  }

  google.maps.event.addListener(openInfoWindow, 'domready', function(){
         
         var title = $('#title');
         var tags = $('#tags');
         var desc = $('#description');
         var radius = $('#radius');
         var lat = $('#latitude');
         var lng = $('#longitude');
         var type = $('#type');
      if (currentMode == 0){
              $('#delete_lost').click(function() {
                  deleteLostInDb(marker);
              });

            $('#edit_lost').click(function() {

              drawCircle(lat,lng,radius,true);
              editLostInDb(marker,title.val(),type.find(":selected").val(),desc.val(),tags.val(),radius.val(),lat.val(),lng.val());
              
              openInfoWindow.close();

               marker['title'] = title.val();
               marker['type'] = type.val();
               marker['description'] = desc.val();
               marker['tags'] = tags.val();
               marker['radius'] = radius.val();
               marker['infowindow'] = new google.maps.InfoWindow({
                  content: "<div id='title'>"+ title.val() +"</div>"
                  + "<div>Description:"+desc.val()+"</div>"
                  + "<div>Tags:"+tags.val()+"</div>"
                });

          
         });
       } else {
          $('#delete_found').click(function() {
                  deleteFoundInDb(marker);
              });


        $('#edit_found').click(function() {
          
            editFoundInDb(marker,title.val(),type.find(":selected").val(),desc.val(),tags.val(),lat.val(),lng.val());
            openInfoWindow.close();

             marker['title'] = title.val();
             marker['type'] = type.val();
             marker['description'] = desc.val();
             marker['tags'] = tags.val();
             marker['infowindow'] = new google.maps.InfoWindow({
                content: "<div id='title'>"+ title.val() +"</div>"
                + "<div>Description:"+desc.val()+"</div>"
                + "<div>Tags:"+tags.val()+"</div>"
                + "<div>Found by:"+marker['username']+"</div>"
              });

          });
          
         }
         
});

  
}


function getCookie(name)
  {
    var re = new RegExp(name + "=([^;]+)");
    var value = re.exec(document.cookie);
    return (value != null) ? unescape(value[1]) : null;
  }

function placeLostMarker(objectID,lat,lng,title,type,description,tags,radius){
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng), 
        map: map,
        draggable:true,
        icon: 'images/lostphones.png'
    });

  marker ['infowindow'] = new google.maps.InfoWindow({
    content: "<div id='title'>"+ title +"</div>"
    + "<div>Description:"+description+"</div>"
    + "<div>Tags:"+tags+"</div>"
  });

  marker['id'] = objectID;
  marker['title'] = title;
  marker['description'] = description;
  marker['tags'] = tags;
  marker['type'] = type;
  marker['radius'] = radius;
  marker['mType'] = 0;
  marker['lat'] = lat;
  marker['lng'] = lng;
  marker['circle'] = getMarkerCircle(marker.getPosition()) ;
  //marker['id'] = idMarker;
  //idMarker = idMarker +1;


  google.maps.event.addListener(marker, 'mouseover', function() {
        this['infowindow'].open(map, this);
    });

  google.maps.event.addListener(marker, 'mouseout', function() {
        this['infowindow'].close();
    });

  google.maps.event.addListener(marker, 'dragend', function() 
{
    editLostInDb(marker,title,type,description,tags,radius,marker.getPosition().lat(),marker.getPosition().lng());
    marker['circle'].setCenter(marker.getPosition());
});

  lostMarkers.push(marker);

  openInfoWindow.close();
}

function placeFoundMarker(objectID,lat,lng,title,type,description,tags,username){
  var marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng), 
        map: map,
        draggable:true,
        animation: google.maps.Animation.DROP,
        icon: 'images/foundphones.png'
    });

      marker ['infowindow'] = new google.maps.InfoWindow({
    content: "<div id='title'>"+ title +"</div>"
    + "<div>Description:"+description+"</div>"
    + "<div>Tags:"+tags+"</div>"
    + "<div>Found by:"+username+"</div>"
  });

  marker['user'] = username;

  google.maps.event.addListener(marker, 'mouseover', function() {
        this['infowindow'].open(map, this);
    });

  google.maps.event.addListener(marker, 'mouseout', function() {
        this['infowindow'].close();
    });

google.maps.event.addListener(marker,'click', function(){
  contactUser(username);
});

google.maps.event.addListener(marker, 'dragend', function() 
{
     editFoundInDb(marker,title,type,description,tags,marker.getPosition().lat(),marker.getPosition().lng());
});

  marker['id'] = objectID;
  marker['title'] = title;
  marker['description'] = description;
  marker['tags'] = tags;
  marker['type'] = type;
  marker['polled'] = 0;
  marker['type'] = type;
  marker['mType'] = 1;
  marker['lat'] = lat;
  marker['lng'] = lng;
  marker['username'] = username;
  //marker['id'] = idMarker;
  //idMarker = idMarker + 1;

  foundMarkers.push(marker);

  openInfoWindow.close();

}

function contactUser(userName){
   $(".modal-content").html("");
   $("<div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button> <h4 class='modal-title' id='myModalLabel'>Contact "+ userName +"</h4></div><div class='modal-body'><button class='btn btn-success'>Contact by Phone</button><button class='btn btn-primary'>Contact by Email</button></div><div class='modal-footer'><button type='button' class='btn btn-danger' data-dismiss='modal'>Close</button></div>").appendTo(".modal-content");
   $("#contactModal").modal('show');
}

function inputFile(){
  $("#inputModal").modal('show');
}

function uploadXML() {
  var file = $("#file_path")[0].files[0];
  if (!file) {
    return;
  }
  var reader = new FileReader();
  reader.readAsText(file);
  reader.onload = function(e) {
    var contents = e.target.result;
    treatXML($.parseXML(contents));
  };

}

function treatXML(xml){
  var markersl = xml.documentElement.getElementsByTagName("lost_object");
      for (var i = 0; i < markersl.length; i++) {
        var title = markersl[i].getAttribute("title");
        var type = markersl[i].getAttribute("type");
        var description = markersl[i].getAttribute("description");
        var tags = markersl[i].getAttribute("tags");
        var radius = markersl[i].getAttribute("radius");
        var lat = markersl[i].getAttribute("lat");
        var longt = markersl[i].getAttribute("longt");
       
        
        var objectID = insertLostInDb(title,type,description,tags,radius,lat,longt);
        drawCircle(lat,longt,radius,true);
        placeLostMarker(objectID,lat,longt,title,description,tags,radius);
        
      }

  var markersf = xml.documentElement.getElementsByTagName("found_object");
      for (var i = 0; i < markersf.length; i++) {
        var title = markersf[i].getAttribute("title");
        var type = markersf[i].getAttribute("type");
        var description = markersf[i].getAttribute("description");
        var tags = markersf[i].getAttribute("tags"); 
        var lat = markersf[i].getAttribute("lat");
        var longt = markersf[i].getAttribute("longt");
        var userName = markersf[i].getAttribute("foundby");

        var objectID = insertFoundInDb(title,type,description,tags,lat,longt);
        placeFoundMarker(objectID,lat,longt,title,description,tags,userName);
      }


      
}



function handleNoGeolocation(errorFlag) {
  if (errorFlag) {
    var content = 'Error: The Geolocation service failed.';
  } else {
    var content = 'Error: Your browser doesn\'t support geolocation.';
  }

  var options = {
    map: map,
    position: new google.maps.LatLng(38.983090, -9.117088),
    content: content
  };

  var infowindow = new google.maps.InfoWindow(options);
}

function loadScript() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://maps.googleapis.com/maps/api/js?libraries=geometry,drawing&key=AIzaSyB0t00mazIDspCaqJ2v_piUXSFbYEErfmo&callback=initialize';
  document.body.appendChild(script);

}

function goHome(){
  map.setCenter(currentPos);
}

function drawCircle(lat,lng, radius, isFinal){
  cRadius = radius;
  if (!isFinal){
    clearCircle(circle);
  } 
  origin = new google.maps.LatLng(lat, lng);
   var radiusOps = {
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      zIndex: 1,
      map: map,
      clickable:false,
      center: origin,
      radius: Number(radius)
    };
    circle = new google.maps.Circle(radiusOps);

    if (isFinal){
      tmp = getMarkerCircle(origin);
      if (tmp == null){
        circles.push(circle);
      } else {
        tmp.setRadius(Number(radius));
      }
    }
    if (radius > 10){
    map.setCenter(radiusOps.center);
    map.fitBounds(circle.getBounds());
    }
}

window.onload = loadScript;